package br.com.estudiofalkor.entity.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CountriesResponse(
    @Json(name = "results")
    val results: Results = Results()
)