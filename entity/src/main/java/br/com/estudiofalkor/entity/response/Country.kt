package br.com.estudiofalkor.entity.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class Country(
    @Json(name = "alpha3")
    val alpha3: String = "",
    @Json(name = "currencyId")
    val currencyId: String = "",
    @Json(name = "currencyName")
    val currencyName: String = "",
    @Json(name = "currencySymbol")
    val currencySymbol: String = "",
    @Json(name = "id")
    val id: String = "",
    @Json(name = "name")
    val name: String = ""
)