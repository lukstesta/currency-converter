package br.com.estudiofalkor.provider.api

import br.com.estudiofalkor.entity.response.CountriesResponse
import retrofit2.Call
import retrofit2.http.GET

interface CountriesApi {

    @GET("countries")
    fun getCountries(): Call<CountriesResponse>
}