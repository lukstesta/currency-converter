package br.com.estudiofalkor.provider.api

import br.com.estudiofalkor.provider.api.configuration.API
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    single { get<Retrofit>(named(API)).create(CountriesApi::class.java) } bind CountriesApi::class
}