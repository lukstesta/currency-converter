package provider

import org.koin.dsl.bind
import org.koin.dsl.module
import provider.impl.CountriesApiProviderImpl

val providerModule = module {
    single { CountriesApiProviderImpl(get()) } bind CountriesApiProvider::class
}