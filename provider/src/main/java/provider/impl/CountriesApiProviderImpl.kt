package provider.impl

import br.com.estudiofalkor.entity.response.CountriesResponse
import br.com.estudiofalkor.provider.api.CountriesApi
import br.com.estudiofalkor.provider.api.extension.fetch
import provider.CountriesApiProvider

class CountriesApiProviderImpl(private val countriesApi: CountriesApi) : CountriesApiProvider {

    override fun getCountries() = countriesApi.getCountries().fetch()
}