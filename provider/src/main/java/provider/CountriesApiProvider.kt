package provider

import br.com.estudiofalkor.entity.response.CountriesResponse

interface CountriesApiProvider {

    fun getCountries() : CountriesResponse
}