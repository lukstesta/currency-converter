package br.com.estudiofalkor.currencyconverter

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import br.com.estudiofalkor.currencyconverter.rule.EspressoCoroutineRule
import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BaseTest {

    @Rule
    @JvmField
    val coroutineRule = EspressoCoroutineRule()

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

}