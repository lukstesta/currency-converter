package br.com.estudiofalkor.currencyconverter.di

import br.com.estudiofalkor.currencyconverter.errorHandling.ErrorHandlingEventManager
import org.koin.dsl.module

val errorHandlingModule = module {
    single { ErrorHandlingEventManager(get()) }
}