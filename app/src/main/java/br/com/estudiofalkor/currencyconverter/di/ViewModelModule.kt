package br.com.estudiofalkor.currencyconverter.di

import br.com.estudiofalkor.currencyconverter.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get(), get()) }
}