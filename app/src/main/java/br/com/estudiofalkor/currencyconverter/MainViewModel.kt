package br.com.estudiofalkor.currencyconverter

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import br.com.estudiofalkor.business.CountriesBusiness
import br.com.estudiofalkor.business.data.handle
import br.com.estudiofalkor.entity.response.CountriesResponse
import br.com.estudiofalkor.infrastructure.coroutines.callAsync

class MainViewModel(
    application: Application,
    private val countriesBusiness: CountriesBusiness
) : AndroidViewModel(application) {

    lateinit var countries: CountriesResponse

    init {
        fetchCountries()
    }

    private fun fetchCountries() = callAsync {
        countriesBusiness.getCountriesAsync().await()
            .handle {
                success {
                    countries = this
                }
                failure {

                }
            }
    }
}
