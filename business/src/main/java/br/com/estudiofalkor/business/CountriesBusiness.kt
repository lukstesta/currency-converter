package br.com.estudiofalkor.business

import br.com.estudiofalkor.business.data.Resource
import br.com.estudiofalkor.entity.response.CountriesResponse
import kotlinx.coroutines.Deferred

interface CountriesBusiness {
    fun getCountriesAsync(): Deferred<Resource<CountriesResponse>>
}