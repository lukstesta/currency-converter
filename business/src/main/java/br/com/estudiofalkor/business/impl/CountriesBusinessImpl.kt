package br.com.estudiofalkor.business.impl

import br.com.estudiofalkor.business.CountriesBusiness
import br.com.estudiofalkor.business.data.Resource
import br.com.estudiofalkor.business.data.resource
import br.com.estudiofalkor.entity.response.CountriesResponse
import br.com.estudiofalkor.infrastructure.coroutines.callAsync
import provider.CountriesApiProvider

class CountriesBusinessImpl(
    private val countriesApiProvider: CountriesApiProvider
) : CountriesBusiness {

    override fun getCountriesAsync() =
        callAsync<Resource<CountriesResponse>> {
            resource { countriesApiProvider.getCountries() }
        }
}