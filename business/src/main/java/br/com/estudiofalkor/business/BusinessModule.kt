package br.com.estudiofalkor.business

import br.com.estudiofalkor.business.impl.CountriesBusinessImpl
import org.koin.dsl.bind
import org.koin.dsl.module

val businessModule = module {
    single { CountriesBusinessImpl(get()) } bind CountriesBusiness::class
}